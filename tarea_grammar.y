%include {
    #include <assert.h>
    #include "tokens.h"
    #include "ast.h"

    int yylex();
    extern int yylineno;

    void yyerror(const char *msg)
    {
        printf("Line %d %s\n",yylineno,msg);
    }
}

%token_type {Token*}

%type input_src             {Statement*}
%type stmt                  {Statement*}
%type assign                {Statement*}
%type print                 {Statement*}
%type stmts                 {Statement*}
%type if_stmt               {Statement*}
%type while_stmt            {Statement*}
%type optional_else         {Statement*}
%type optional_block_stmt   {Statement*}

%type expr                  {Expr*}
%type term                  {Expr*}
%type factor                {Expr*}
%type logic_expression      {Expr*}

%type print_format          {int}
%type logic_operator        {int}

%default_type {int}

%name KjParse

%start_symbol input_src

input_src(R)::= stmts(Ss).        { R = Ss; Ss->exec(); }


stmts(R)::= stmts(Ss) TK_EOL stmt(S) .     { R = Ss; ((BlockStatement*)R)->addStatement(S); }
stmts(R)::= stmt(S) .                         { R = new BlockStatement; ((BlockStatement*)R)->addStatement(S); }

eols::= eols TK_EOL .
eols::= TK_EOL .

opt_eols::= eols .
opt_eols::= .

stmt(R)::= assign(A) .      { R = A; }
stmt(R)::= print(P) .       { R = P; }
stmt(R)::= if_stmt(I) .     { R = I; }
stmt(R)::= while_stmt(W) .  { R = W; }

if_stmt(R)::= KW_IF TK_LEFT_PAR logic_expression(L) TK_RIGHT_PAR eols optional_block_stmt(IB) optional_else(EB) . { R = new IfStatement(L,IB,EB); }

while_stmt(R)::= KW_WHILE TK_LEFT_PAR logic_expression(L) TK_RIGHT_PAR TK_EOL optional_block_stmt(B) .   { R = new WhileStatement(L,B); }

optional_else(R)::= KW_ELSE eols optional_block_stmt(EB) . { R = EB; }
optional_else::= .

optional_block_stmt(R)::= TK_LEFT_CURLY_BRAKET opt_eols stmts(Ss) opt_eols TK_RIGHT_CURLY_BRAKET . { R = Ss; }
optional_block_stmt(R)::= stmt(S) .     { R = S; }

logic_expression(R)::= expr(E1) logic_operator(LO) expr(E2) . { R = new LogicalExpression(E1,LO,E2); }

logic_operator(R)::= OP_EQUAL .              { R = EQUAL; }
logic_operator(R)::= OP_DISTINCT .           { R = DISTINCT; }
logic_operator(R)::= OP_GRATER_EQUAL_THAN .  { R = GRATER_EQUAL_THAN; }
logic_operator(R)::= OP_LESS_EQUAL_THAN .    { R = LESS_EQUAL_THAN; }
logic_operator(R)::= OP_GRATER_THAN .        { R = GRATER_THAN; }
logic_operator(R)::= OP_LESS_THAN .          { R = LESS_THAN; }

assign(R)::= KW_VARINDEX(V) OP_ASSIGN expr(E) .    { R = new AssignStatement(V->str_value,E); }

print(R)::= KW_PRINT expr(E) PT_COMMA print_format(PF) .  { R = new PrintStatement(E,PF); }
print(R)::= KW_PRINT expr(E) .                            { R = new PrintStatement(E,DEC);}

print_format(R)::= KW_HEX .   { R = HEX; }
print_format(R)::= KW_BIN .   { R = BIN; }
print_format(R)::= KW_DEC .   { R = DEC; }

expr(R)::= expr(E) OP_ADD term(T) .  { R = new AddExpr(E,T); }
expr(R)::= expr(E) OP_SUB term(T) .  { R = new SubExpr(E,T); }
expr(R)::= term(T) .                 { R = T; }

term(R)::= term(T) OP_MUL factor(F) .    { R = new MulExpr(T,F); }
term(R)::= term(T) OP_DIV factor(F) .    { R = new DivExpr(T,F); }
term(R)::= factor(F) .                   { R = F; }

factor(R)::= TK_NUMBER(N) .                       { R = new NumberExpr(N->int_value); }
factor(R)::= KW_VARINDEX(V) .                     { R = new VarExpr(V->str_value); }
factor(R)::= TK_LEFT_PAR expr(E) TK_RIGHT_PAR .   { R = E; }