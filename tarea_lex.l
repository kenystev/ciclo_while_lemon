%option noyywrap
%option yylineno

%x multiline_comment
%x include

%{
	#include <string.h>
	#include <stdio.h>
	#include "tokens.h"
	#include "ast.h"

	typedef struct include_stack
	{
		struct include_stack *prev;
		FILE *f;
		int lineno;
		char *filename;
		YY_BUFFER_STATE bs;
	} include_stack_t;

	include_stack_t *inc_stack = NULL;
	char *yyfilename;
	Token *current_token;
	int push_file(char *filename);
	int pop_file();
%}

%%
[ \t]	 {}
"/*"	 					{ BEGIN(multiline_comment); }
"//".*\n 					{ return TK_EOL; }
"=="	 					{ return OP_EQUAL; }
"!="	 					{ return OP_DISTINCT; }
">="	 					{ return OP_GRATER_EQUAL_THAN; }
"<="	 					{ return OP_LESS_EQUAL_THAN; }
">"	 						{ return OP_GRATER_THAN; }
"<"	 						{ return OP_LESS_THAN; }
","		 					{ return PT_COMMA; }
"+"		 					{ return OP_ADD; }
"-"		 					{ return OP_SUB; }
"*"		 					{ return OP_MUL; }
"/"		 					{ return OP_DIV; }
"hex"		 					{ return KW_HEX; }
"bin"		 					{ return KW_BIN; }
"dec"		 					{ return KW_DEC; }
"if"		 					{ return KW_IF; }
"else"		 					{ return KW_ELSE; }
"while"							{ return KW_WHILE; }
^"#"[ \t]*"include"             { BEGIN(include); }
"="		 	{ return OP_ASSIGN; }
")"		 	{ return TK_RIGHT_PAR; }
"("		 	{ return TK_LEFT_PAR; }
"}"		 	{ return TK_RIGHT_CURLY_BRAKET; }
"{"		 	{ return TK_LEFT_CURLY_BRAKET; }
"print"	 	{ return KW_PRINT; }
[0-9]+	 	{ current_token = new Token(NULL, atoi(yytext)); return TK_NUMBER; }
(_[A-Za-z]+)?[A-Za-z0-9_]* 	{ current_token = new Token(new string(yytext),-1); return KW_VARINDEX; }
"\n"	 	{ return TK_EOL; }
<INITIAL><<EOF>>	 	{ if(pop_file()!=0) yyterminate(); return TK_EOL; }
.		 	{ printf("unexpected token '%s' at line: %d", yytext, yylineno); }

<include>[ \t]*                 { /* Nothing */ }
<include>\".*\"               {
	    {
	    	yytext++;
		    yytext[strlen(yytext)-1] = '\0';

		    int c;
		    while ((c = yyinput()) == ' ' || c == '\t');
		    if(c != '\n'){
		        printf("Line %d: Bad include expression line '-> %s <-'\n", yylineno, yytext);
		        return 0;
		    }
		    yylineno++;

		    if(push_file(yytext) != 0)
		        yyterminate();
		    BEGIN(INITIAL);
	    }
    }
<include>"<".*">"            {
    {
    	yytext++;
	    yytext[strlen(yytext)-1] = '\0';

	    int c;
	    while ((c = yyinput()) == ' ' || c == '\t');
	    if(c != '\n'){
	        printf("Line %d: Bad include expression line\n", yylineno);
	        return 0;
	    }

	    if(push_file(yytext) != 0)
	        yyterminate();
	    BEGIN(INITIAL);
    }
}

<multiline_comment>"*/" { BEGIN(INITIAL); }
<multiline_comment>.|\n {/*NADA*/}
<multiline_comment><<EOF>> {printf("Line %d: Block comment without close.\n",yylineno); return 0;}

%%

int push_file(char *filename){
	FILE* f = fopen(filename, "r");

    if (f == NULL)
    {
        fprintf(stderr, "Cannot open file %s\n", filename);
        return 1;
    }

    include_stack_t* is = (include_stack_t*)(malloc(sizeof(include_stack_t)));
    if(is == NULL){
        fprintf(stderr, "Out of memory\n");
        return 1;
    }

    is->f = f;
    is->lineno = yylineno;
    is->bs = yy_create_buffer(f, YY_BUF_SIZE);
    is->prev = inc_stack;
    is->filename = yyfilename;

    inc_stack = is;

    yylineno = 1;
    yyfilename = filename;

    yy_switch_to_buffer(is->bs);

    return 0;
}

int pop_file(){
	include_stack_t* prev = inc_stack->prev;
    fclose(inc_stack->f);
    yy_delete_buffer(inc_stack->bs);

    yylineno = inc_stack->lineno;
    yyfilename = inc_stack->filename;

    free(inc_stack);

    if(prev == NULL) return 1;

    yy_switch_to_buffer(prev->bs);
    inc_stack = prev;

    return 0;
}
