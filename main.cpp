#include <stdio.h>
#include "ast.h"
#include "tokens.h"

extern FILE *yyin;
int push_file(char *);

void* KjParseAlloc(void* (*allocProc)(size_t));
void* KjParse(void*, int, Token*);
void* KjParseFree(void*, void(*freeProc)(void*));

int yylex();
extern Token *current_token;

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s <input file>[]\n", argv[0]);
        return 1;
    }

    if (push_file(argv[1]) != 0)
        return 1;

    void *parser = KjParseAlloc(malloc);
    int token = yylex();

    while (token != 0)
    {
        if(token == TK_EOL){
            while( (token = yylex()) == TK_EOL );
            if(token == 0) break;

            if(token == KW_ELSE){
                KjParse(parser, KW_ELSE, 0);
            }else{
                KjParse(parser, TK_EOL, 0);
                KjParse(parser, token, current_token);
            }
        }else{
            KjParse(parser, token, current_token);
        }
        token = yylex();
    }
    KjParse(parser, 0, 0);

    KjParseFree(parser, free);
}